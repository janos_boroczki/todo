import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { SaveTodoRequest } from '../todo/save-todo-request';
import { TodoFormComponent } from '../todo/todo-form/todo-form.component';
import { TodoService } from '../todo/todo.service';

@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

	constructor(private dialog: MatDialog,
				private todoService: TodoService) { }

	ngOnInit(): void {
		
	}

	public newTodo(): void {
		let dialogRef: MatDialogRef<TodoFormComponent> = this.dialog.open(TodoFormComponent);

	    dialogRef.afterClosed().subscribe((data: SaveTodoRequest) => {
			if (data !== undefined && data !== null) {
		      	this.todoService.createTodo(data);
			}
	    });
	}	

}
