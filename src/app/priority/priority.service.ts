import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { Priority } from './priority';

@Injectable({
	providedIn: 'root'
})
export class PriorityService {

	private BASE_URL: string = '/api/priority';
	
	private priorities: Subject<Priority[]> = new Subject<Priority[]>();

	constructor(private httpClient: HttpClient) { }
	
	public getPriorities(): Observable<Priority[]> {
		
		this.httpClient
			.get(this.BASE_URL)
			.pipe(map(result => <Priority[]> result))
			.subscribe((priorities: Priority[]) => this.priorities.next(priorities));
		
		return this.priorities;
	}
}
