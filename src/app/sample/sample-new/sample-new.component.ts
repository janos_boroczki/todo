import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { SampleService } from '../sample.service';

@Component({
	selector: 'sample-new',
	templateUrl: './sample-new.component.html',
	styleUrls: ['./sample-new.component.scss']
})
export class SampleNewComponent implements OnInit {

	public sampleForm: FormGroup = new FormGroup({
		name: new FormControl('', [Validators.required])
	});

	constructor(private sampleService: SampleService) { }

	ngOnInit(): void {
	}

	public save(): void {
		if (this.sampleForm.invalid) {
			this.sampleForm.markAllAsTouched();
		} else {
			this.sampleService.saveSample({
				name: this.sampleForm.controls['name'].value
			});
			this.sampleForm.reset();
		}
	}

}
