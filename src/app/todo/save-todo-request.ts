export interface SaveTodoRequest {
	
	name: string;
	
	priority: number;
	
	deadline?: Date;
}
