import { Component, Input, OnInit } from '@angular/core';
import { Direction } from '../direction';
import { TodoService } from '../todo.service';

@Component({
	selector: 'sort-arrow',
	templateUrl: './sort-arrow.component.html',
	styleUrls: ['./sort-arrow.component.scss']
})
export class SortArrowComponent implements OnInit {

	@Input()
	public sortPath: string = "";

	constructor(private todoService: TodoService) { }

	ngOnInit(): void {
	}

		
	public orderedByAsc(sortPath: string): boolean {
		return this.todoService.orderedBy(sortPath, Direction.ASC);
	}
	
	public orderedByDesc(sortPath: string): boolean {
		return this.todoService.orderedBy(sortPath, Direction.DESC);
	}

}
