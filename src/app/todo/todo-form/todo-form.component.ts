import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { Priority } from 'src/app/priority/priority';
import { PriorityService } from 'src/app/priority/priority.service';
import { SaveTodoRequest } from '../save-todo-request';
import { Todo } from '../todo';
import * as moment from 'moment';

@Component({
	selector: 'todo-form',
	templateUrl: './todo-form.component.html',
	styleUrls: ['./todo-form.component.scss']
})
export class TodoFormComponent implements OnInit {

	public priorities: Observable<Priority[]> = new Observable<Priority[]>();

	public todoForm: FormGroup = new FormGroup({
		name: new FormControl('', [Validators.required]),
		priority: new FormControl('', [Validators.required]),
		deadline: new FormControl('', [])
	});

	constructor(private priorityService: PriorityService,
				private dialogRef: MatDialogRef<TodoFormComponent>,
				@Inject(MAT_DIALOG_DATA) public data: Todo) { }

	public ngOnInit(): void {
		this.priorities = this.priorityService.getPriorities();
		
		if (this.data !== undefined && this.data !== null) {
			this.todoForm.controls['name'].setValue(this.data.name);
			this.todoForm.controls['priority'].setValue(this.data.priority.id);
			if (this.data.deadline !== undefined && this.data.deadline !== null) {
				this.todoForm.controls['deadline'].setValue(moment(this.data.deadline));
			}
		}
	}
	
	public cancel(): void {
		this.dialogRef.close();
	}
	
	public save(): void {
		if (this.todoForm.invalid) {
			this.todoForm.markAllAsTouched();
		} else {
			
			let saveTodoRequest: SaveTodoRequest = {
				name: this.todoForm.controls['name'].value,
				priority: this.todoForm.controls['priority'].value
			};
			
			let deadline: any = this.todoForm.controls['deadline'].value;
			
			if (deadline !== undefined && deadline !== null && deadline !== '') {
				saveTodoRequest.deadline = deadline.toDate();
			}
			
			this.dialogRef.close(saveTodoRequest);
		}
	}

}
