export interface TodoListFilters {
	
	name?: string;

	priorityId?: number;

	deadlineFrom?: Date;

	deadlineTo?: Date;

	creationDateFrom?: Date;

	creationDateTo?: Date;
	
}
