import { TodoListFilters } from './todo-list-filters';
import { TodoListSort } from './todo-list-sort';

export interface TodoListRequest {
	
	sortList: TodoListSort[];

	filters: TodoListFilters;

	offset: number;

	limit: number;
	
}
