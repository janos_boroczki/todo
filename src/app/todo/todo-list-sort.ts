import { Direction } from './direction';

export interface TodoListSort {
	
	path: string;

	direction: Direction;
	
}
