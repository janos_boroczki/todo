import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { SaveTodoRequest } from '../save-todo-request';
import { Todo } from '../todo';
import { TodoFormComponent } from '../todo-form/todo-form.component';
import { TodoService } from '../todo.service';

@Component({
	selector: 'todo-list',
	templateUrl: './todo-list.component.html',
	styleUrls: ['./todo-list.component.scss']
})
export class TodoListComponent implements OnInit {

	public todoList: Observable<Todo[]> = new Observable<Todo[]>();
	
	constructor(private todoService: TodoService,
				private dialog: MatDialog) { }

	public ngOnInit(): void {
		this.todoList = this.todoService.getTodos();
	}
	
	public sortBy(sortPath: string): void {
		this.todoService.sortBy(sortPath);
	}
	
	public editTodo(todo: Todo): void {
		let dialogRef: MatDialogRef<TodoFormComponent> = this.dialog.open(TodoFormComponent, {
			data: todo
		});

	    dialogRef.afterClosed().subscribe((data: SaveTodoRequest) => {
			if (data !== undefined && data !== null) {
		    	this.todoService.updateTodo(<number> todo.id, data);
			}
	    });
	}
	
	public deleteTodo(todo: Todo): void {
		this.todoService.deleteTodo(<number> todo.id);
	}
	
	public previousPage(): void {
		this.todoService.previousPage();
	}
	
	public nextPage(): void {
		this.todoService.nextPage();
	}
	
}
