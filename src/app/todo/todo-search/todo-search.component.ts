import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { Priority } from 'src/app/priority/priority';
import { PriorityService } from 'src/app/priority/priority.service';
import { TodoListFilters } from '../todo-list-filters';
import { TodoService } from '../todo.service';

@Component({
	selector: 'todo-search',
	templateUrl: './todo-search.component.html',
	styleUrls: ['./todo-search.component.scss']
})
export class TodoSearchComponent implements OnInit {

	public todoSearchForm: FormGroup = new FormGroup({
		name: new FormControl(''),
		priority: new FormControl(''),
		deadline: new FormGroup({
			from: new FormControl(''),
			to: new FormControl('')
		}),
		creationDate: new FormGroup({
			from: new FormControl(''),
			to: new FormControl('')
		})
	});
	
	public priorities: Observable<Priority[]> = new Observable<Priority[]>();

	constructor(private todoService: TodoService,
				private priorityService: PriorityService) { }

	ngOnInit(): void {
		this.priorities = this.priorityService.getPriorities();
	}

	public search(): void { 
		
		let filters: TodoListFilters = {};
		
		let name: string = this.todoSearchForm.controls['name'].value
		
		if (name !== undefined && name !== null && name !== "") {
			filters.name = name;
		}
		
		let priority: number = this.todoSearchForm.controls['priority'].value;
		
		if (priority !== undefined && priority !== null) {
			filters.priorityId = priority;
		}
		
		let deadlineFrom: Date = this.todoSearchForm.get('deadline.from')?.value;
		
		if (deadlineFrom !== undefined && deadlineFrom !== null) {
			filters.deadlineFrom = deadlineFrom;
		}
		
		let deadlineTo: Date = this.todoSearchForm.get('deadline.to')?.value;
		
		if (deadlineTo !== undefined && deadlineTo !== null) {
			filters.deadlineTo = deadlineTo;
		}
		
		let creationDateFrom: Date = this.todoSearchForm.get('creationDate.from')?.value;
		
		if (creationDateFrom !== undefined && creationDateFrom !== null) {
			filters.creationDateFrom = creationDateFrom;
		}
		
		let creationDateTo: Date = this.todoSearchForm.get('creationDate.to')?.value;
		
		if (creationDateTo !== undefined && creationDateTo !== null) {
			filters.creationDateTo = creationDateTo;
		}
		
		this.todoService.filter(filters);
	}
	
}
