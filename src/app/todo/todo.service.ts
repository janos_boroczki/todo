import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Direction } from './direction';
import { SaveTodoRequest } from './save-todo-request';
import { Todo } from './todo';
import { TodoListFilters } from './todo-list-filters';
import { TodoListRequest } from './todo-list-request';
import { TodoListSort } from './todo-list-sort';

@Injectable({
	providedIn: 'root'
})
export class TodoService {

	private readonly BASE_URL: string = '/api/todo';
	
	private readonly PAGE_SIZE: number = 10;

	private todoListSubject: Subject<Todo[]> = new Subject<Todo[]>();
	
	private todoListRequestSubject: Subject<TodoListRequest> = new Subject<TodoListRequest>();
	
	private todoListRequest: TodoListRequest = {
												 sortList: [
														  {
															path: "priority.priority",
															direction: Direction.ASC
														  }
														],
												 filters: {},
												 offset: 0,
												 limit: this.PAGE_SIZE
												};

	constructor(private httpClient: HttpClient) {
		this.todoListRequestSubject
			.subscribe((todoListRequest: TodoListRequest) => this.getList(todoListRequest));
			
		this.todoListRequestSubject.next(this.todoListRequest);
		
		this.todoListRequestSubject.subscribe((todoListRequest: TodoListRequest) => this.todoListRequest = todoListRequest);
	}

	private getList(todoListRequest: TodoListRequest): void {
		this.httpClient.post(this.BASE_URL + "/list", todoListRequest)
						.pipe(map(result => <Todo[]> result))
						.subscribe((todos: Todo[]) => {
							if (todos.length === 0 && this.todoListRequest.offset > 0) {
								this.todoListRequest.offset = this.todoListRequest.offset - this.PAGE_SIZE;
								this.getList(this.todoListRequest);
							} else {
								this.todoListSubject.next(todos);
							}
							
						});
	}
	
	public getTodos(): Observable<Todo[]> {
		return this.todoListSubject;
	}
	
	public sortBy(sortPath: string): void {
		let sortElem: TodoListSort | undefined = this.todoListRequest
									     .sortList
										 .find((sortElem: TodoListSort) => sortElem.path === sortPath);
		
		if (sortElem === undefined) {
			this.todoListRequest.sortList.push({
				path: sortPath,
				direction: Direction.ASC
			});
			
		} else if (sortElem.direction === Direction.ASC) {

			sortElem.direction = Direction.DESC;
			
		} else {
			let index: number = this.todoListRequest
									.sortList
									.indexOf(sortElem);
			
			this.todoListRequest
				.sortList
				.splice(index, 1);
		}
		
		this.resetOffset();
		
		this.todoListRequestSubject
			.next(this.todoListRequest);
	}
	
	private resetOffset(): void {
		this.todoListRequest.offset = 0;
	}
	
	public orderedBy(sortPath: string, direction: Direction): boolean {
		return this.todoListRequest
			.sortList
			.findIndex((sortElem: TodoListSort) => sortElem.path === sortPath && sortElem.direction === direction) >= 0;
	}
	
	public filter(filters: TodoListFilters): void {
		this.todoListRequest.filters = filters;
		
		this.resetOffset();
		
		this.todoListRequestSubject
			.next(this.todoListRequest);
	}
	
	public createTodo(saveTodoRequest: SaveTodoRequest): void {
		this.httpClient
			.post(this.BASE_URL, saveTodoRequest)
			.subscribe(() => this.getList(this.todoListRequest));
	}
	
	public updateTodo(id: number, saveTodoRequest: SaveTodoRequest): void {
		this.httpClient
			.put(this.BASE_URL + '/' + id, saveTodoRequest)
			.subscribe(() => this.getList(this.todoListRequest));
	}
	
	public deleteTodo(id: number): void {
		this.httpClient
			.delete(this.BASE_URL + '/' + id)
			.subscribe(() => this.getList(this.todoListRequest));
	}
	
	public previousPage(): void {
		if (this.todoListRequest.offset > 0) {
			this.todoListRequest.offset = this.todoListRequest.offset - this.PAGE_SIZE;
			this.todoListRequestSubject.next(this.todoListRequest);
		}
	}
	
	public nextPage(): void {
		this.todoListRequest.offset = this.todoListRequest.offset + this.PAGE_SIZE;
		this.todoListRequestSubject.next(this.todoListRequest);
	}
	
}
