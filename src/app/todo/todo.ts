import { Priority } from '../priority/priority';

export interface Todo {
	
	id?: number;
	
	name: string;

	priority: Priority;

	deadline: Date;

	creationDate: Date;
}
