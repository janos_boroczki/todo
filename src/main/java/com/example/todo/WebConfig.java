package com.example.todo;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConfig implements WebMvcConfigurer {

	@Override
	public void addViewControllers(ViewControllerRegistry registry) {
		WebMvcConfigurer.super.addViewControllers(registry);
		registry.addViewController("/").setViewName("forward:/index.html");
	}

//	@Override
//	public void addResourceHandlers(ResourceHandlerRegistry registry) {
//		WebMvcConfigurer.super.addResourceHandlers(registry);
////		registry.addResourceHandler("{filename:\\w+\\.css}")
////        	.addResourceLocations("/");
//
//		registry.addResourceHandler("{filename:.*\\.js}")
//			.addResourceLocations("/");
////        	.addResourceLocations("classpath:/WEB-INF/classes/static/");
//	}
	
}
