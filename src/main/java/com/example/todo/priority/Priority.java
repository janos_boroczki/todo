package com.example.todo.priority;

import javax.persistence.Entity;
import javax.persistence.Table;

import org.springframework.data.jpa.domain.AbstractPersistable;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "priorities")
public class Priority extends AbstractPersistable<Integer> {

	private String name;

	private Integer priority;

	@Override
	public void setId(Integer id) {
		super.setId(id);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getPriority() {
		return priority;
	}

	public void setPriority(Integer priority) {
		this.priority = priority;
	}

	@JsonIgnore
	@Override
	public boolean isNew() {
		return super.isNew();
	}

	@Override
	public String toString() {
		return "Priority [id=" + getId() + ", name=" + name + ", priority=" + priority + "]";
	}

}
