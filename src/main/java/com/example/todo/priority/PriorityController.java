package com.example.todo.priority;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/priority")
public class PriorityController {

	@Autowired
	private PriorityRepository priorityRepository;
	
	@GetMapping
	public List<Priority> getPriorities() {
		return priorityRepository.findAll(Sort.by("priority"));
	}
	
}
