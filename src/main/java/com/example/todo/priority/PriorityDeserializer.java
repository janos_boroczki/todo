package com.example.todo.priority;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jackson.JsonComponent;

import com.fasterxml.jackson.core.JacksonException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

@JsonComponent
public class PriorityDeserializer extends JsonDeserializer<Priority> {

	private static final Logger logger = LoggerFactory.getLogger(PriorityDeserializer.class);
	
	@Autowired
	private PriorityRepository priorityRepository;

	@Override
	public Priority deserialize(JsonParser jsonParser, DeserializationContext context)
			throws IOException, JacksonException {

		int priorityId = jsonParser.getIntValue();

		Priority priority = priorityRepository.findById(priorityId)
											  .orElse(null);
		
		logger.debug("Priority [{}] deserialized from id [{}]", priority, priorityId);
		
		return priority;
	}

}
