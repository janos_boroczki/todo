package com.example.todo.todo;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;

public enum PredicateType {

	STARTS_WITH {
		@Override
		public Predicate predicate(CriteriaBuilder criteriaBuilder, Expression<?> expression, Object value) {
			return criteriaBuilder.like((Expression<String>) expression, ((String) value) + "%");
		}
	},
	EQUALS {
		@Override
		public Predicate predicate(CriteriaBuilder criteriaBuilder, Expression<?> expression, Object value) {
			return criteriaBuilder.equal(expression, value);
		}
	},
	GREATER_THAN_OR_EQUAL_TO {
		@Override
		public Predicate predicate(CriteriaBuilder criteriaBuilder, Expression<?> expression, Object value) {
			if (value instanceof LocalDate) {
				return criteriaBuilder.greaterThanOrEqualTo((Expression<? extends LocalDate>) expression, (LocalDate) value);
			} else if (value instanceof LocalDateTime) {
				return criteriaBuilder.greaterThanOrEqualTo((Expression<? extends LocalDateTime>) expression, (LocalDateTime) value);
			} else {
				throw new UnsupportedOperationException("GREATER_THAN_OR_EQUAL is not supported by type [{" + value.getClass().getName() + "}]");
			}
		}
	},
	LESS_THAN_OR_EQUAL_TO {
		@Override
		public Predicate predicate(CriteriaBuilder criteriaBuilder, Expression<?> expression, Object value) {
			if (value instanceof LocalDate) {
				return criteriaBuilder.lessThanOrEqualTo((Expression<? extends LocalDate>) expression, (LocalDate) value);
			} else if (value instanceof LocalDateTime) {
				return criteriaBuilder.lessThanOrEqualTo((Expression<? extends LocalDateTime>) expression, (LocalDateTime) value);
			} else {
				throw new UnsupportedOperationException("LESS_THAN_OR_EQUAL is not supported by type [{" + value.getClass().getName() + "}]");
			}
		}
	};
	
	public abstract Predicate predicate(CriteriaBuilder criteriaBuilder, Expression<?> expression, Object value);
}
