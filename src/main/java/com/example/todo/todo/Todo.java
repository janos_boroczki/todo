package com.example.todo.todo;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.data.jpa.domain.AbstractPersistable;

import com.example.todo.priority.Priority;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;

@Entity
@Table(name = "todos")
public class Todo extends AbstractPersistable<Integer> {

	@JsonView(SaveTodoView.class)
	@NotNull
	@NotEmpty
	private String name;

	@NotNull
	@JsonView(SaveTodoView.class)
	@ManyToOne
	@JoinColumn(name = "priority_id")
	private Priority priority;

	@JsonView(SaveTodoView.class)
	private LocalDate deadline;

	@Column(name = "creation_date")
	private LocalDateTime creationDate;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Priority getPriority() {
		return priority;
	}

	public void setPriority(Priority priority) {
		this.priority = priority;
	}

	public LocalDate getDeadline() {
		return deadline;
	}

	public void setDeadline(LocalDate deadline) {
		this.deadline = deadline;
	}

	public LocalDateTime getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(LocalDateTime creationDate) {
		this.creationDate = creationDate;
	}

	@JsonIgnore
	@Override
	public boolean isNew() {
		return super.isNew();
	}

	@Override
	public String toString() {
		return "Todo [id=" + getId() + ", name=" + name + ", deadline=" + deadline
				+ ", creationDate=" + creationDate + "]";
	}

}
