package com.example.todo.todo;

import java.time.Clock;
import java.time.LocalDateTime;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.example.todo.priority.Priority;
import com.example.todo.priority.PriorityDeserializer;
import com.fasterxml.jackson.annotation.JsonView;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/api/todo")
public class TodoController {
	
	private static final Logger logger = LoggerFactory.getLogger(TodoController.class);
	
	@Autowired
	private Clock clock;
	
	@Autowired
	private TodoRepository todoRepository;
	
	/**
	 * Create a {@link Todo} item, with this HTTP POST method path: /api/todo.
	 * {@link Todo} fields from the client are:
	 *  - Name (mandatory), this is a short summary of the {@link Todo} item.
	 *  - Priority (mandatory), this is an id which refers a {@link Priority} entity, it shows which {@link Todo} is more important then others.
	 *    This field is deserialized by {@link PriorityDeserializer}.
	 *  - Deadline (optional), this is the deadline of the {@link Todo}
	 * Other automatically generated {@link Todo} fields are:
	 *  - Id, the primary key of the item in database
	 *  - CreationDate, time of the saving
	 * 
	 *  
	 * @param todo is the entity from the request body
	 * @return
	 * 
	 * Possible HTTP Responses:
	 *  - 200 OK, if successfully created a new {@link Todo} item  
	 *  - 400 Bad Request, if request body is invalid
	 *  - 500 Internal Server Error, in case of unexpected error
	 * 
	 */
	@PostMapping
	public void createTodo(@RequestBody @JsonView(SaveTodoView.class) @Valid Todo todo) {
		logger.debug("Creating Todo item [{}]", todo);
		
		todo.setCreationDate(LocalDateTime.now(clock));
		
		todo = todoRepository.save(todo);
		
		logger.debug("Todo item [{}] has been created", todo);
	}
	
	/**
	 * Get all {@link Todo} items this HTTP POST method path: /api/todo. 
	 * Request body is {@link TodoListRequest} which contains sorting, filtering 
	 * and paging informations. 
	 * 
	 * The request body could looks similar: 
	 * 
	 * {
	 *   "offset": 0,
	 *   "limit": 10,
	 *   "sortList": [
	 *     {
	 *       "path": "priority.priority",
	 *       "direction": "ASC"
	 *     },
	 *     {
	 *       "path": "name",
	 *       "direction": "DESC"
	 *     }
	 *   ],
	 *   "filters": {
	 *     "name": "foo",
	 *     "priorityId": 1,
	 *     "deadlineFrom": "2022-01-01",
	 *     "deadlineTo": "2022-01-04",
	 *     "creationDateFrom": "2022-03-26T11:00:01",
	 *     "creationDateTo": "2022-03-26T14:30:01"
	 *   }
	 * }
	 * 
	 * Where {@link TodoListRequest} represents the json request body, in which fields are:
	 *  - offset (required, minimum is {@value 0} is the starting index of the result list (first index is {@value 0}
	 *  - limit (required, minimum is {@value 1} is the size of result list
	 *  - sortList (optional) contains the properties and direction which used to sort, in order of sorting priority
	 *   - path (required, if parent is present), path for the property in the json of {@link Todo}
	 *   - direction (required, if parent is present), could be ASC or DESC
	 *  - filters (optional) contains the filter information for the specified properties
	 *   - name (optional) is a starts with filter for name property of {@link Todo}
	 *   - priorityId is an exact match filter for id property of {@link Priority} of {@link Todo}
	 *   - deadlineFrom is a greater than or equal to filter for deadline property of {@link Todo}
	 *   - deadlineTo is a less than or equal to filter for deadline property of {@link Todo}
	 *   - creationDateFrom is a greater than or equal to filter for creationDate property of {@link Todo}
	 *   - creationDateTo is a less than or equal to filter for creationDate property of {@link Todo}
	 *   
	 * @param request is the {@link TodoListRequest} class representation of the json request.
	 * @return with non blocking {@link Flux<Todo>}, which contains the requested {@link Todo} items
	 * 
	 * Possible HTTP Responses:
	 *  - 200 OK, if successfully returns with list of {@link Todo} items
	 *  - 400 Bad Request, if request body is invalid
	 *  - 500 Internal Server Error, in case of unexpected error 
	 */
	@PostMapping("/list")
	public Flux<Todo> list(@RequestBody @Valid TodoListRequest request) {
		logger.debug("Getting Todo items with [{}]", request);
		
		return Mono.fromCallable(() -> request)
				   .map(todoRepository::list)
				   .flatMapMany(Flux::fromStream);
	}
	
	/**
	 * Update a {@link Todo} item based on id url parameter, with this HTTP PUT method path: /api/todo/{id}.
	 * {@link Todo} fields from the client are:
	 *  - Name (mandatory), this is a short summary of the {@link Todo} item.
	 *  - Priority (mandatory), this is an id which refers a {@link Priority} entity, it shows which {@link Todo} is more important then others.
	 *    This field is deserialized by {@link PriorityDeserializer}.
	 *  - Deadline (optional), this is the deadline of the {@link Todo}
	 * Other non modifiable {@link Todo} fields are:
	 *  - Id, the primary key of the item in database
	 *  - CreationDate is not changed by update
	 * 
	 * @param id is the identity of updatable {@link Todo} entity
	 * @param todo object contains the new values of updatable {@link Todo} entity
	 * @return
	 * 
	 * Possible HTTP Responses:
	 *  - 200 OK, if successfully updated the {@link Todo} item by id 
	 *  - 400 Bad Request, if request body is invalid, or {@link Todo} does not exist with id
	 *  - 500 Internal Server Error, in case of unexpected error
	 * 
	 */
	@Transactional
	@PutMapping("/{id}")
	public void update(@PathVariable("id") Integer id,
					   @RequestBody @JsonView(SaveTodoView.class) @Valid Todo todo) {
		
		logger.debug("Updating Todo item [{}] with [{}]", id, todo);
		
		Todo existingTodo = getTodoById(id);
		
		existingTodo.setName(todo.getName());
		existingTodo.setPriority(todo.getPriority());
		existingTodo.setDeadline(todo.getDeadline());
		existingTodo = todoRepository.save(existingTodo);
		
		logger.debug("Todo [{}] updated", existingTodo);
	}

	private Todo getTodoById(Integer id) {
		return todoRepository.findById(id)
							 .orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST));
	}
	
	/**
	 * Delete a {@link Todo} item based on id url parameter, with this HTTP DELETE method path: /api/todo/{id}.
	 * 
	 * @param id is the identity of deleteable {@link Todo} entity
	 * * @return
	 * 
	 * Possible HTTP Responses:
	 *  - 200 OK, if successfully deleted the {@link Todo} item by id
	 *  - 400 Bad Request, if {@link Todo} does not exist with id
	 *  - 500 Internal Server Error, in case of unexpected error
	 */
	@DeleteMapping("/{id}")
	public void delete(@PathVariable("id") Integer id) {
		logger.debug("Deleting Todo item by id [{}]", id);
		
		Todo existingTodo = getTodoById(id);
		
		todoRepository.delete(existingTodo);
		
		logger.debug("Todo item have been deleted");
	}

}
