package com.example.todo.todo;

public class TodoListFilter {

	private String path;

	private PredicateType predicateType;

	private Object predicateValue;

	public TodoListFilter(String path, PredicateType predicateType, Object predicateValue) {
		super();
		this.path = path;
		this.predicateType = predicateType;
		this.predicateValue = predicateValue;
	}

	public String getPath() {
		return path;
	}

	public PredicateType getPredicateType() {
		return predicateType;
	}

	public Object getPredicateValue() {
		return predicateValue;
	}
	
	@Override
	public String toString() {
		return "TodoListFilter [path=" + path + ", predicateType=" + predicateType + ", predicateValue="
				+ predicateValue + "]";
	}

}
