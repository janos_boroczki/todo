package com.example.todo.todo;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.stream.Stream;

public class TodoListFilters {

	private String name;

	private Integer priorityId;

	private LocalDate deadlineFrom;

	private LocalDate deadlineTo;

	private LocalDateTime creationDateFrom;

	private LocalDateTime creationDateTo;

	public TodoListFilters() {
		super();
	}
	
	public TodoListFilters(String name, Integer priorityId, LocalDate deadlineFrom, LocalDate deadlineTo,
			LocalDateTime creationDateFrom, LocalDateTime creationDateTo) {
		super();
		this.name = name;
		this.priorityId = priorityId;
		this.deadlineFrom = deadlineFrom;
		this.deadlineTo = deadlineTo;
		this.creationDateFrom = creationDateFrom;
		this.creationDateTo = creationDateTo;
	}



	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getPriorityId() {
		return priorityId;
	}

	public void setPriorityId(Integer priorityId) {
		this.priorityId = priorityId;
	}

	public LocalDate getDeadlineFrom() {
		return deadlineFrom;
	}

	public void setDeadlineFrom(LocalDate deadlineFrom) {
		this.deadlineFrom = deadlineFrom;
	}

	public LocalDate getDeadlineTo() {
		return deadlineTo;
	}

	public void setDeadlineTo(LocalDate deadlineTo) {
		this.deadlineTo = deadlineTo;
	}

	public LocalDateTime getCreationDateFrom() {
		return creationDateFrom;
	}

	public void setCreationDateFrom(LocalDateTime creationDateFrom) {
		this.creationDateFrom = creationDateFrom;
	}

	public LocalDateTime getCreationDateTo() {
		return creationDateTo;
	}

	public void setCreationDateTo(LocalDateTime creationDateTo) {
		this.creationDateTo = creationDateTo;
	}
	
	public Stream<TodoListFilter> getFilters() {
		return Stream.of(new TodoListFilter("name", PredicateType.STARTS_WITH, name),
						new TodoListFilter("priority.id", PredicateType.EQUALS, priorityId),
						new TodoListFilter("deadline", PredicateType.GREATER_THAN_OR_EQUAL_TO, deadlineFrom),
						new TodoListFilter("deadline", PredicateType.LESS_THAN_OR_EQUAL_TO, deadlineTo),
						new TodoListFilter("creationDate", PredicateType.GREATER_THAN_OR_EQUAL_TO, creationDateFrom),
						new TodoListFilter("creationDate", PredicateType.LESS_THAN_OR_EQUAL_TO, creationDateTo))
					.filter(listFilter -> listFilter.getPredicateValue() != null);
	}

	@Override
	public String toString() {
		return "TodoListFilter [name=" + name + ", priorityId=" + priorityId + ", deadlineFrom=" + deadlineFrom
				+ ", deadlineTo=" + deadlineTo + ", creationDateFrom=" + creationDateFrom + ", creationDateTo="
				+ creationDateTo + "]";
	}

}
