package com.example.todo.todo;

import java.util.stream.Stream;

public interface TodoListRepository {

	Stream<Todo> list(TodoListRequest todoListRequest);
	
}
