package com.example.todo.todo;

import java.util.stream.Stream;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

public class TodoListRepositoryImpl implements TodoListRepository {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public Stream<Todo> list(TodoListRequest todoListRequest) {
		
		try (Session session = sessionFactory.openSession()) {
			CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
			
			CriteriaQuery<Todo> query = criteriaBuilder.createQuery(Todo.class);
			
			Root<Todo> todo = query.from(Todo.class);
			
			query.select(todo);
			
			if (todoListRequest.getFilters() != null) {
				Predicate[] predicates = todoListRequest.getFilters()
						.getFilters()
						.map(filter -> this.predicate(criteriaBuilder, todo, filter))
						.toArray(Predicate[]::new);
				
				query.where(criteriaBuilder.and(predicates));
			}
			
			if (todoListRequest.getSortList() != null) {
				
				Order[] orders = todoListRequest.getSortList()
						.stream()
						.map(sort -> this.order(criteriaBuilder, todo, sort))
						.toArray(Order[]::new);
				
				query.orderBy(orders);
			}
			
			EntityManager entityManager = null;
			
			try {
				
				entityManager = sessionFactory.createEntityManager();
				
				return entityManager.createQuery(query)
						.setFirstResult(todoListRequest.getOffset())
						.setMaxResults(todoListRequest.getLimit())
						.getResultList()
						.stream();
			} finally {
				 if (entityManager != null) {
					 entityManager.close();
				 }
			}
		}
		
	}
	
	private Predicate predicate(CriteriaBuilder criteriaBuilder, 
							   Root<Todo> todo, 
							   TodoListFilter filter) {
		
		return filter.getPredicateType()
					 .predicate(criteriaBuilder, 
							 	convertPath(todo, filter.getPath()),
							 	filter.getPredicateValue());
	}
	
	private Path<?> convertPath(Root<Todo> todo, String pathString) {
		String[] pathElements = pathString.split("\\.");
		
		Path<?> path = todo.get(pathElements[0]);
		
		for (int i = 1; i < pathElements.length; i++) {
			path = path.get(pathElements[i]);
		}
		
		return path;
	}
	
	
	private Order order(CriteriaBuilder criteriaBuilder, 
					   Root<Todo> todo, 
					   TodoListSort sort) {
		
		Path<?> path = convertPath(todo, sort.getPath());
		
		return sort.getDirection().isAscending() ? criteriaBuilder.asc(path) : criteriaBuilder.desc(path);
	}

}
