package com.example.todo.todo;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class TodoListRequest {

	@Valid
	private List<TodoListSort> sortList;

	@Valid
	private TodoListFilters filters;

	@NotNull
	@Min(0)
	private Integer offset;

	@NotNull
	@Min(1)
	private Integer limit;

	public List<TodoListSort> getSortList() {
		return sortList;
	}

	public void setSortList(List<TodoListSort> sortList) {
		this.sortList = sortList;
	}

	public TodoListFilters getFilters() {
		return filters;
	}

	public void setFilters(TodoListFilters filters) {
		this.filters = filters;
	}

	public Integer getOffset() {
		return offset;
	}

	public void setOffset(Integer offset) {
		this.offset = offset;
	}

	public Integer getLimit() {
		return limit;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}

	@Override
	public String toString() {
		return "TodoListRequest [sortList=" + sortList + ", filters=" + filters + ", offset=" + offset + ", limit="
				+ limit + "]";
	}

}
