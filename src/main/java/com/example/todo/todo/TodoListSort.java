package com.example.todo.todo;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.data.domain.Sort;

public class TodoListSort {

	@NotNull
	@NotEmpty
	private String path;

	@NotNull
	private Sort.Direction direction;

	public TodoListSort() {
		super();
	}

	public TodoListSort(@NotNull @NotEmpty String path, @NotNull Sort.Direction direction) {
		super();
		this.path = path;
		this.direction = direction;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public Sort.Direction getDirection() {
		return direction;
	}

	public void setDirection(Sort.Direction direction) {
		this.direction = direction;
	}

	@Override
	public String toString() {
		return "TodoListSort [path=" + path + ", direction=" + direction + "]";
	}

}
