CREATE TABLE public.priorities
(
    id integer,
    name character varying(32) not null,
    priority integer not null,
    PRIMARY KEY (id)
);

insert into public.priorities (id, name, priority) values (nextval('hibernate_sequence'), 'LOW', 100);
insert into public.priorities (id, name, priority) values (nextval('hibernate_sequence'), 'MEDIUM', 50);
insert into public.priorities (id, name, priority) values (nextval('hibernate_sequence'), 'HIGH', 10);