CREATE TABLE public.todos
(
    id integer,
    name text not null,
    priority_id integer not null,
    deadline date,
    creation_date timestamp without time zone NOT NULL, 
    PRIMARY KEY (id)
);