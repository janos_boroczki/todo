package com.example.todo.todo;

import java.time.Clock;
import java.time.LocalDateTime;
import java.time.ZoneId;

import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;

@TestConfiguration
public class TestConfig {

	public static final LocalDateTime CREATION_TIME = LocalDateTime.of(2022, 1, 1, 10, 23);
	
	@Bean
    public Clock clock() {
        return Clock.fixed(CREATION_TIME.atZone(ZoneId.systemDefault()).toInstant(), ZoneId.systemDefault());
    }
	
	@Bean
	public TodoControllerTestDataProvider todoControllerTestDataProvider() {
		return new TodoControllerTestDataProvider();
	}
	
}
