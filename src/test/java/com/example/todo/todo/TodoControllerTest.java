package com.example.todo.todo;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.test.web.reactive.server.WebTestClient;

import com.example.todo.priority.Priority;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT, 
				properties = "spring.main.allow-bean-definition-overriding=true",
				classes = {TestConfig.class})
@AutoConfigureWebTestClient
public class TodoControllerTest {
	
	private static final String TODO_PATH = "/api/todo";
	
	private static final String TODO_LIST_PATH = "/api/todo/list";

	private static final String TODO_EMPTY_NAME = "";

	private static final String TODO_NAME = "something";

	private static final int WRONG_PRIORITY = -1;
	
	@Autowired
	private WebTestClient client;
	
	@Autowired
	private TodoRepository todoRepository;
	
	@Autowired
	private TodoControllerTestDataProvider dataProvider;

	private Priority priority;
	
	@BeforeEach
	public void init() {
		dataProvider.cleanUp();
		
		priority = dataProvider.createTestPriority("LOW", 100);
	}
	
	@Test
	public void testCreateTodoWithNullName() {
		this.client.post()
				   .uri(TODO_PATH)
				   .bodyValue(dataProvider.getCreateTodoRequest(null, priority.getId(), null))
				   .exchange()
				   .expectStatus()
				   .isBadRequest();
	}
	
	@Test
	public void testCreateTodoWithEmptyName() {
		this.client.post()
				   .uri(TODO_PATH)
				   .bodyValue(dataProvider.getCreateTodoRequest(TODO_EMPTY_NAME, priority.getId(), null))
				   .exchange()
				   .expectStatus()
				   .isBadRequest();
	}
	
	@Test
	public void testCreateTodoWithNotEmptyNameEmptyPriority() {
		this.client.post()
				   .uri(TODO_PATH)
				   .bodyValue(dataProvider.getCreateTodoRequest(TODO_NAME, null, null))
				   .exchange()
				   .expectStatus()
				   .isBadRequest();
	}
	
	@Test
	public void testCreateTodoWithNotEmptyNameWrongPriority() {
		this.client.post()
				   .uri(TODO_PATH)
				   .bodyValue(dataProvider.getCreateTodoRequest(TODO_NAME, WRONG_PRIORITY, null))
				   .exchange()
				   .expectStatus()
				   .isBadRequest();
	}
	
	@Test
	public void testCreateTodoWithNotEmptyNameCorrectPriorityWithoutDeadline() {
		this.client.post()
				   .uri(TODO_PATH)
				   .bodyValue(dataProvider.getCreateTodoRequest(TODO_NAME, priority.getId(), null))
				   .exchange()
				   .expectStatus()
				   .isOk();
		
		List<Todo> savedTodos = this.todoRepository.findAll();
		
		assertEquals(1, savedTodos.size());
		assertEquals(null, savedTodos.get(0).getDeadline());
		assertEquals(TODO_NAME, savedTodos.get(0).getName());
		assertEquals(priority, savedTodos.get(0).getPriority());
		assertEquals(TestConfig.CREATION_TIME, savedTodos.get(0).getCreationDate());
	}
	
	@Test
	public void testCreateTodoWithNotEmptyNameCorrectPriorityWithDeadline() {
		LocalDate deadline = LocalDate.of(2022, 12, 1);
		
		this.client.post()
				   .uri(TODO_PATH)
				   .bodyValue(dataProvider.getCreateTodoRequest(TODO_NAME, priority.getId(), deadline))
				   .exchange()
				   .expectStatus()
				   .isOk();
		
		List<Todo> savedTodos = this.todoRepository.findAll();
		
		assertEquals(1, savedTodos.size());
		assertEquals(deadline, savedTodos.get(0).getDeadline());
		assertEquals(TODO_NAME, savedTodos.get(0).getName());
		assertEquals(priority, savedTodos.get(0).getPriority());
		assertEquals(TestConfig.CREATION_TIME, savedTodos.get(0).getCreationDate());
	}
	
	@Test
	public void testListWithoutOffset() {
		this.client.post()
				   .uri(TODO_LIST_PATH)
				   .bodyValue(dataProvider.getTodoListRequest(null, 1))
				   .exchange()
				   .expectStatus()
				   .isBadRequest();
	}
	
	@Test
	public void testListWithInvalidOffset() {
		this.client.post()
				   .uri(TODO_LIST_PATH)
				   .bodyValue(dataProvider.getTodoListRequest(-1, 1))
				   .exchange()
				   .expectStatus()
				   .isBadRequest();
	}
	
	@Test
	public void testListWithoutLimit() {
		this.client.post()
				   .uri(TODO_LIST_PATH)
				   .bodyValue(dataProvider.getTodoListRequest(0, null))
				   .exchange()
				   .expectStatus()
				   .isBadRequest();
	}
	
	@Test
	public void testListWithInvalidLimit() {
		this.client.post()
				   .uri(TODO_LIST_PATH)
				   .bodyValue(dataProvider.getTodoListRequest(0, 0))
				   .exchange()
				   .expectStatus()
				   .isBadRequest();
	}
	
	@Test
	public void testListWithValidOffsetAndLimit() {
		dataProvider.createTodoList(1);
		
		this.client.post()
				   .uri(TODO_LIST_PATH)
				   .bodyValue(dataProvider.getTodoListRequest(0, 1))
				   .exchange()
				   .expectStatus()
				   .isOk()
				   .expectBody()
				   .jsonPath("$")
				   .value(hasSize(1))
				   .jsonPath("$[0].id")
				   .exists()
				   .jsonPath("$[0].name")
				   .value(is("name0"))
				   .jsonPath("$[0].priority.name")
				   .value(is("LOW"))
				   .jsonPath("$[0].priority.priority")
				   .value(is(75))
				   .jsonPath("$[0].deadline")
				   .value(is("2022-04-01"))
				   .jsonPath("$[0].creationDate")
				   .value(is("2022-03-27T04:00:00"));
	}
	
	@Test
	public void testListWithValidOffsetAndLimitAndSortWithoutPath() {
		this.client.post()
		   .uri(TODO_LIST_PATH)
		   .bodyValue(dataProvider.getTodoListRequest(0, 1, new TodoListSort(null, Direction.ASC)))
		   .exchange()
		   .expectStatus()
		   .isBadRequest();
	}
	
	@Test
	public void testListWithValidOffsetAndLimitAndSortWithWrongPath() {
		this.client.post()
		   .uri(TODO_LIST_PATH)
		   .bodyValue(dataProvider.getTodoListRequest(0, 1, new TodoListSort("wrongpath", Direction.ASC)))
		   .exchange()
		   .expectStatus()
		   .is5xxServerError();
	}
	
	@Test
	public void testListWithValidOffsetAndLimitAndSortWithCorrectPathWithoutDirection() {
		this.client.post()
		   .uri(TODO_LIST_PATH)
		   .bodyValue(dataProvider.getTodoListRequest(0, 1, new TodoListSort("creationDate", null)))
		   .exchange()
		   .expectStatus()
		   .isBadRequest();
	}
	
	@Test
	public void testListWithOffset2AndLimit3AndMultiSort() {
		dataProvider.createTodoList(6);
		
		this.client.post()
		   .uri(TODO_LIST_PATH)
		   .bodyValue(dataProvider.getTodoListRequest(2, 3, new TodoListSort("priority.priority", Direction.ASC), new TodoListSort("deadline", Direction.ASC)))
		   .exchange()
		   .expectStatus()
		   .isOk()
		   .expectBody()
		   .jsonPath("$")
		   .value(hasSize(3))
		   .jsonPath("$[0].id")
		   .exists()
		   .jsonPath("$[0].name")
		   .value(is("name1"))
		   .jsonPath("$[0].priority.name")
		   .value(is("MEDIUM"))
		   .jsonPath("$[0].priority.priority")
		   .value(is(25))
		   .jsonPath("$[0].deadline")
		   .value(is("2022-04-02"))
		   .jsonPath("$[0].creationDate")
		   .value(is("2022-03-27T04:30:00"))
		   .jsonPath("$[1].id")
		   .exists()
		   .jsonPath("$[1].name")
		   .value(is("name4"))
		   .jsonPath("$[1].priority.name")
		   .value(is("MEDIUM"))
		   .jsonPath("$[1].priority.priority")
		   .value(is(25))
		   .jsonPath("$[1].deadline")
		   .value(is("2022-04-05"))
		   .jsonPath("$[1].creationDate")
		   .value(is("2022-03-27T06:00:00"))
		   .jsonPath("$[2].id")
		   .exists()
		   .jsonPath("$[2].name")
		   .value(is("name0"))
		   .jsonPath("$[2].priority.name")
		   .value(is("LOW"))
		   .jsonPath("$[2].priority.priority")
		   .value(is(75))
		   .jsonPath("$[2].deadline")
		   .value(is("2022-04-01"))
		   .jsonPath("$[2].creationDate")
		   .value(is("2022-03-27T04:00:00"));
	}
	
	@Test
	public void testListWithOffset2AndLimit1AndSortByCreationDateDescAndFilterByName() {
		dataProvider.createTodoList(50);
		
		this.client.post()
		   .uri(TODO_LIST_PATH)
		   .bodyValue(dataProvider.getTodoListRequest(2, 1, new TodoListFilters("name1", null, null, null, null, null), new TodoListSort("creationDate", Direction.DESC)))
		   .exchange()
		   .expectStatus()
		   .isOk()
		   .expectBody()
		   .jsonPath("$")
		   .value(hasSize(1))
		   .jsonPath("$[0].id")
		   .exists()
		   .jsonPath("$[0].name")
		   .value(is("name17"))
		   .jsonPath("$[0].priority.name")
		   .value(is("HIGH"))
		   .jsonPath("$[0].priority.priority")
		   .value(is(5))
		   .jsonPath("$[0].deadline")
		   .value(is("2022-04-18"))
		   .jsonPath("$[0].creationDate")
		   .value(is("2022-03-27T12:30:00"));
	}
	
	@Test
	public void testListWithOffset0AndLimit2AndSortByCreationDateDescAndFilterByPriorityId() {
		Integer priorityId = dataProvider.createTodoList(2)
										 .get(0)
										 .getPriority()
										 .getId();
		
		this.client.post()
		   .uri(TODO_LIST_PATH)
		   .bodyValue(dataProvider.getTodoListRequest(0, 2, new TodoListFilters(null, priorityId, null, null, null, null), new TodoListSort("creationDate", Direction.DESC)))
		   .exchange()
		   .expectStatus()
		   .isOk()
		   .expectBody()
		   .jsonPath("$")
		   .value(hasSize(1))
		   .jsonPath("$[0].id")
		   .exists()
		   .jsonPath("$[0].name")
		   .value(is("name0"))
		   .jsonPath("$[0].priority.name")
		   .value(is("LOW"))
		   .jsonPath("$[0].priority.priority")
		   .value(is(75))
		   .jsonPath("$[0].deadline")
		   .value(is("2022-04-01"))
		   .jsonPath("$[0].creationDate")
		   .value(is("2022-03-27T04:00:00"));
	}
	
	@Test
	public void testListWithOffset0AndLimit10AndSortByCreationDateDescAndFilterByDeadline() {
		dataProvider.createTodoList(50);
		
		this.client.post()
		   .uri(TODO_LIST_PATH)
		   .bodyValue(dataProvider.getTodoListRequest(0, 10, new TodoListFilters(null, null, LocalDate.of(2022, 4, 10), LocalDate.of(2022, 4, 11), null, null), new TodoListSort("creationDate", Direction.DESC)))
		   .exchange()
		   .expectStatus()
		   .isOk()
		   .expectBody()
		   .jsonPath("$")
		   .value(hasSize(2))
		   .jsonPath("$[0].id")
		   .exists()
		   .jsonPath("$[0].name")
		   .value(is("name10"))
		   .jsonPath("$[0].priority.name")
		   .value(is("MEDIUM"))
		   .jsonPath("$[0].priority.priority")
		   .value(is(25))
		   .jsonPath("$[0].deadline")
		   .value(is("2022-04-11"))
		   .jsonPath("$[0].creationDate")
		   .value(is("2022-03-27T09:00:00"))
		   .jsonPath("$[1].id")
		   .exists()
		   .jsonPath("$[1].name")
		   .value(is("name9"))
		   .jsonPath("$[1].priority.name")
		   .value(is("LOW"))
		   .jsonPath("$[1].priority.priority")
		   .value(is(75))
		   .jsonPath("$[1].deadline")
		   .value(is("2022-04-10"))
		   .jsonPath("$[1].creationDate")
		   .value(is("2022-03-27T08:30:00"));
	}
	
	@Test
	public void testListWithOffset0AndLimit10AndSortByCreationDateDescAndFilterByCreationDate() {
		dataProvider.createTodoList(50);
		
		this.client.post()
		   .uri(TODO_LIST_PATH)
		   .bodyValue(dataProvider.getTodoListRequest(0, 10, new TodoListFilters(null, null, null, null, LocalDateTime.of(2022, 03, 27, 8, 30, 0), LocalDateTime.of(2022, 03, 27, 9, 0, 0)), new TodoListSort("creationDate", Direction.DESC)))
		   .exchange()
		   .expectStatus()
		   .isOk()
		   .expectBody()
		   .jsonPath("$")
		   .value(hasSize(2))
		   .jsonPath("$[0].id")
		   .exists()
		   .jsonPath("$[0].name")
		   .value(is("name10"))
		   .jsonPath("$[0].priority.name")
		   .value(is("MEDIUM"))
		   .jsonPath("$[0].priority.priority")
		   .value(is(25))
		   .jsonPath("$[0].deadline")
		   .value(is("2022-04-11"))
		   .jsonPath("$[0].creationDate")
		   .value(is("2022-03-27T09:00:00"))
		   .jsonPath("$[1].id")
		   .exists()
		   .jsonPath("$[1].name")
		   .value(is("name9"))
		   .jsonPath("$[1].priority.name")
		   .value(is("LOW"))
		   .jsonPath("$[1].priority.priority")
		   .value(is(75))
		   .jsonPath("$[1].deadline")
		   .value(is("2022-04-10"))
		   .jsonPath("$[1].creationDate")
		   .value(is("2022-03-27T08:30:00"));
	}
	
	@Test
	public void testUpdateTodoWithNullName() {
		this.client.put()
				   .uri(TODO_PATH + "/1")
				   .bodyValue(dataProvider.getCreateTodoRequest(null, priority.getId(), null))
				   .exchange()
				   .expectStatus()
				   .isBadRequest();
	}
	
	@Test
	public void testUpdateTodoWithEmptyName() {
		this.client.put()
				   .uri(TODO_PATH + "/1")
				   .bodyValue(dataProvider.getCreateTodoRequest("", priority.getId(), null))
				   .exchange()
				   .expectStatus()
				   .isBadRequest();
	}
	
	@Test
	public void testUpdateTodoWithValidNameWithoutPriority() {
		this.client.put()
				   .uri(TODO_PATH + "/1")
				   .bodyValue(dataProvider.getCreateTodoRequest("asd", null, null))
				   .exchange()
				   .expectStatus()
				   .isBadRequest();
	}
	
	@Test
	public void testUpdateNonExistingTodoWithValidNameValidPriority() {
		this.client.put()
				   .uri(TODO_PATH + "/1")
				   .bodyValue(dataProvider.getCreateTodoRequest("asd", priority.getId(), null))
				   .exchange()
				   .expectStatus()
				   .isBadRequest();
	}
	
	@Test
	public void testExistingUpdateTodoWithValidNameAndValidPriority() {
		Todo existingTodo = dataProvider.createTodoList(1).get(0);
		
		String updatedName = "asd";
		LocalDate updatedDeadline = LocalDate.of(2022, 4, 15);
		
		this.client.put()
				   .uri(TODO_PATH + "/" + existingTodo.getId())
				   .bodyValue(dataProvider.getCreateTodoRequest(updatedName, priority.getId(), updatedDeadline))
				   .exchange()
				   .expectStatus()
				   .isOk();
		
		Todo updatedTodo = todoRepository.findById(existingTodo.getId()).get();
		
		assertNotEquals(existingTodo.getName(), updatedTodo.getName());
		assertEquals(updatedName, updatedTodo.getName());
		assertNotEquals(existingTodo.getPriority(), updatedTodo.getPriority());
		assertEquals(priority, updatedTodo.getPriority());
		assertNotEquals(existingTodo.getDeadline(), updatedTodo.getDeadline());
		assertEquals(updatedDeadline, updatedTodo.getDeadline());
		assertEquals(existingTodo.getCreationDate(), updatedTodo.getCreationDate());
	}
	
	@Test
	public void testNonExistingDelete() {
		this.client.delete()
				   .uri(TODO_PATH + "/1")
				   .exchange()
				   .expectStatus()
				   .isBadRequest();
	}
	
	@Test
	public void testExistingDelete() {
		Todo existingTodo = dataProvider.createTodoList(1).get(0);
		
		this.client.delete()
				   .uri(TODO_PATH + "/" + existingTodo.getId())
				   .exchange()
				   .expectStatus()
				   .isOk();
		
		assertEquals(0, todoRepository.count());
	}
	
}
