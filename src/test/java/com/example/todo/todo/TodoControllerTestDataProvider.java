package com.example.todo.todo;

import static java.util.stream.Collectors.toList;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;

import com.example.todo.priority.Priority;
import com.example.todo.priority.PriorityRepository;

public class TodoControllerTestDataProvider {

	@Autowired
	private TodoRepository todoRepository;
	
	@Autowired
	private PriorityRepository priorityRepository;
	
	public void cleanUp() {
		todoRepository.deleteAll();
		priorityRepository.deleteAll();
	}
	
	public Priority createTestPriority(String name,
									   Integer prior) {
		Priority priority = new Priority();
		
		priority.setName(name);
		priority.setPriority(prior);
		
		return priorityRepository.save(priority);
	}
	
	public Map<String, Object> getCreateTodoRequest(String name,
			Integer priority,
			LocalDate deadline) {
		
		Map<String, Object> request = new HashMap<>();
		
		request.put("name", name);
		request.put("priority", priority);
		request.put("deadline", deadline);
		
		return request;
	}
	
	public Map<String, Object> getTodoListRequest(Integer offset,
												  Integer limit) {
		
		Map<String, Object> request = new HashMap<>();
		
		request.put("offset", offset);
		request.put("limit", limit);
		
		return request;
		
	}
	
	public Map<String, Object> getTodoListRequest(Integer offset,
												  Integer limit,
												  TodoListSort... sort) {
		Map<String, Object> request = getTodoListRequest(offset, limit);
		
		request.put("sortList", sort);
		
		return request;
	}
	
	public Map<String, Object> getTodoListRequest(Integer offset,
			  Integer limit,
			  TodoListFilters filters,
			  TodoListSort... sort) {
		Map<String, Object> request = getTodoListRequest(offset, limit, sort);
		
		request.put("filters", filters);
		
		return request;
	}
	
	public List<Todo> createTodoList(int count) {
		LocalDate baseDeadline = LocalDate.of(2022, 4, 1);
		LocalDateTime baseCreationDate = LocalDateTime.of(2022, 3, 27, 4, 0, 0);
		
		List<Priority> testPriorities = Stream.of(createTestPriority("LOW", 75),
												  createTestPriority("MEDIUM", 25),
												  createTestPriority("HIGH", 5))
											  .collect(toList());
		
		return IntStream.range(0, count)
						.mapToObj(index -> this.createTodo(index,
							 						   baseDeadline,
							 						   baseCreationDate,
							 						   testPriorities))
						.collect(toList());
	}
	
	private Todo createTodo(int index,
							LocalDate baseDeadline,
							LocalDateTime baseCreationDate,
							List<Priority> testPriorities) {
		return createTodo("name" + index,
						  baseDeadline.plusDays(index),
						  testPriorities.get(index % 3),
						  baseCreationDate.plusMinutes(index * 30));
	}
	
	private Todo createTodo(String name,
						   LocalDate deadline,
						   Priority priority,
						   LocalDateTime creationDate) {
		Todo todo = new Todo();
		
		todo.setName(name);
		todo.setDeadline(deadline);
		todo.setPriority(priority);
		todo.setCreationDate(creationDate);
		
		return todoRepository.save(todo);
	}
	
}
